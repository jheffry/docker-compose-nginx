# Stage 1 : Build Stage
FROM nginx:alpine AS builder

RUN apk add doas; \
  adduser -D jhef; \
    echo 'jhef:123' | chpasswd; \
    echo 'permit jhef as root' > /etc/doas.d/doas.conf

# Copy the necessary files for building
COPY ./src/ /var/www/html/
COPY default.conf /etc/nginx/conf.d

# Stage 2 : Runtime Stage
FROM nginx:alpine
# Copy files from the builder stages
COPY --from=builder /etc/doas.d/doas.conf etc/doas.d/doas.conf
COPY --from=builder /etc/nginx/conf.d/default.conf etc/nginx/conf.d/default.conf
COPY --from=builder /var/www/html/ /var/www/html/
COPY --from=builder /etc/passwd /etc/passwd

# Set the user to the one created in the builder stage
USER jhef